import 'package:flutter/material.dart';

class Settings extends StatefulWidget {
  @override
  _checkingState createState() => _checkingState();
}

class _checkingState extends State<Settings> {
  List<String> categoryData = ['abc', 'xyz', 'xyz'];
  int _selectedIndex = 0;

  _onSelected(int index) {
    print(index);
    setState(() => _selectedIndex = index);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Settings"),),
      body: Container(
        height: 200,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: categoryData.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                  onTap: () => _onSelected(index),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      width: 68,
                      height: 68,
                      decoration: BoxDecoration(
                        color: _selectedIndex != null && _selectedIndex == index
                            ? Colors.black
                            : Colors.grey,
                        borderRadius: BorderRadius.circular(16),
                        border: Border.all(
                          color: Colors.blue,
                          width: 1,
                        ),
                      ),
                    ),
                  ));
            }),
      ),
    );
  }
}
