import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:module_03/dashboard.dart';
import 'package:module_03/user_registration.dart';

class LogIn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
            colors: [Colors.white70, Colors.blueGrey],
            begin: Alignment.bottomRight,
          )),
          child: Container(
            child: Center(
              child: Container(
                width: 400,
                height: 400,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 50),
                        child: Text(
                          "THE IDEATION HUB",
                          style: TextStyle(
                              color: Colors.deepPurpleAccent,
                              fontWeight: FontWeight.bold,
                              fontSize: 25),
                        )),

                    //Start username
                    Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 100, vertical: 9),
                        child: TextFormField(
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            icon: const Icon(Icons.person),
                            labelText: 'username',
                          ),
                        )), //End username
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 100, vertical: 16),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          border: UnderlineInputBorder(),
                          icon: const Icon(Icons.lock),
                          labelText: 'password',
                        ),
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 30, vertical: 16),
                      child: ElevatedButton(
                        child: const Text("Login"),
                        style: ElevatedButton.styleFrom(
                            fixedSize: const Size(160, 30),
                            primary: Colors.deepOrange,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50))),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Dashboard()),
                          );
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 30, vertical: 9),
                      child: TextButton(
                          child: Text("Register here"),
                          style: TextButton.styleFrom(
                            primary: Colors.blue,
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => UserRegistration()),
                            );
                          }),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
